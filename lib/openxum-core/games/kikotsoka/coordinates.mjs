"use strict";

class Coordinates {
  constructor(c, l) {
    this._column = c;
    this._line = l;
  }

  clone() {
    return new Coordinates(this._column, this._line);
  }

  column() {
    return this._column;
  }

  equals(coordinates) {
    return this._column === coordinates.get_column() &&
      this._line === coordinates.get_line();
  }

  line() {
    return this._line;
  }

  to_string() {
    return String.fromCharCode('A'.charCodeAt(0) + this._column) + (this._line + 1);
  }
}

export default Coordinates;