"use strict";

// namespace Kikotsoka
import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import Engine from './engine.mjs';
import GameType from './game_type.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import Patterns from './patterns.mjs';
import Phase from './phase.mjs';
import State from './state.mjs';

export default {
  Color: Color,
  Coordinates: Coordinates,
  Engine: Engine,
  GameType: GameType,
  Move: Move,
  MoveType: MoveType,
  Patterns: Patterns,
  Phase: Phase,
  State: State
};