"use strict";

let Phase = {FIRST_MOVE: 0, CAPTURE: 1, CHOOSE: 2, SECOND_CAPTURE: 3, MAKE_STRONGER: 4};

export default Phase;