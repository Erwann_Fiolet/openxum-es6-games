require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let e = new OpenXum.Paletto.Engine(OpenXum.Paletto.GameType.STANDARD, OpenXum.Paletto.Color.PLAYER_1);
let p1 = new AI.Generic.RandomPlayer(OpenXum.Paletto.Color.PLAYER_1, OpenXum.Paletto.Color.PLAYER_2, e);
let p2 = new AI.Generic.RandomPlayer(OpenXum.Paletto.Color.PLAYER_2, OpenXum.Paletto.Color.PLAYER_1, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
  let move = p.move();

  moves.push(move);
  e.move(move);
  p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === OpenXum.Paletto.Color.PLAYER_1 ? "player 1" : "player 2"));
for (let index = 0; index < moves.length; ++index) {
  console.log(moves[index].to_string());
}