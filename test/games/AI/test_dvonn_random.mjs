require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let e = new OpenXum.Dvonn.Engine(OpenXum.Dvonn.GameType.STANDARD, OpenXum.Dvonn.Color.RED);
let p1 = new AI.Generic.RandomPlayer(OpenXum.Dvonn.Color.BLACK, OpenXum.Dvonn.Color.WHITE, e);
let p2 = new AI.Generic.RandomPlayer(OpenXum.Dvonn.Color.WHITE, OpenXum.Dvonn.Color.BLACK, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
  let move = p.move();

  moves.push(move);
  e.move(move);
  p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === OpenXum.Dvonn.Color.BLACK ? "black" : "white"));
for (let index = 0; index < moves.length; ++index) {
  console.log(moves[index].to_string());
}