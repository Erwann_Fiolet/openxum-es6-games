require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let victoire = 0;

for (let i = 0; i < 20; i += 1) {
  let e = new OpenXum.Tintas.Engine(OpenXum.Tintas.GameType.STANDARD, OpenXum.Tintas.Color.PLAYER_1);
  let p1 = new AI.Generic.RandomPlayer(OpenXum.Tintas.Color.PLAYER_1, OpenXum.Tintas.Color.PLAYER_2, e);
  let p2 = new AI.Generic.RandomPlayer(OpenXum.Tintas.Color.PLAYER_2, OpenXum.Tintas.Color.PLAYER_1, e);
  let p = p1;
  let moves = [];
  while (!e.is_finished()) {
    let move = p.move();

    moves.push(move);
    e.move(move);
    p = p === p1 ? p2 : p1;
  }
  console.log(e.winner_is());
  if (e.winner_is() === 0) {
    victoire += 1;
  }
}

console.log("victoire", victoire);